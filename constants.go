package sdk

const (
	// EnvCommunity is the default engine URL
	EnvCommunity = "https://api.rainbird.ai"
	// EnvEnterprise is the URL used by premium Enterprise clients
	EnvEnterprise = "https://api-enterprise.rainbird.ai"
)

// Version is the version of this SDK
const Version = "0.7.0"
